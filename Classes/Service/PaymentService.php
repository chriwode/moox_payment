<?php
namespace DCNGmbH\MooxPayment\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\SingletonInterface;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility; 
 
/**
 *
 *
 * @package moox_payment
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PaymentService implements SingletonInterface {
	
	/**
	 * objectManager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager	
	 */
	public $objectManager;		
	
	/**
	 * extConf
	 *
	 * @var boolean
	 */
	public $extConf;
	
	
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_payment/Resources/Private/Language/locallang.xlf:';
	
	/**
     *
     * @return void
     */
    public function initialize() {								
		
		// initialize object manager
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');		
				
		// get extensions's configuration
		$this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_payment']);
				
    }	
	
	/**
	 * get payment methods
	 *	
	 * @param \string|\array $allowed
	 * @param \string|\array $exclude
	 * @return \array $paymentMethods
	 */
	public function getPaymentMethods($allowed = array(), $exclude = array()){
		
		// initialize
		$this->initialize();
		$paymentMethods = array();
		
		if(!is_array($allowed) && $allowed!=""){
			$allowed = explode(",",$allowed);
		} elseif(!is_array($allowed)){
			$allowed = array();
		}
		
		if(!is_array($exclude) && $exclude!=""){
			$exclude = explode(",",$exclude);
		} elseif(!is_array($exclude)){
			$exclude = array();
		}
		
		if(count($allowed)){
			if(in_array('moox_payment_voucher',$allowed) && !in_array('moox_payment_voucher',$exclude)){
				if(ExtensionManagementUtility::isLoaded('moox_payment_voucher')){			
					$paymentProvider = $this->objectManager->get('DCNGmbH\MooxPaymentVoucher\Service\PaymentProvider');		
					if(is_object($paymentProvider)){
						$paymentMethods['moox_payment_voucher'] = $paymentProvider->getPaymentConfig();
					}
				}
			}
			if(in_array('moox_payment_vrpay',$allowed) && !in_array('moox_payment_vrpay',$exclude)){
				if(ExtensionManagementUtility::isLoaded('moox_payment_vrpay')){			
					$paymentProvider = $this->objectManager->get('DCNGmbH\MooxPaymentVrpay\Service\PaymentProvider');		
					if(is_object($paymentProvider)){
						$paymentMethods['moox_payment_vrpay'] = $paymentProvider->getPaymentConfig();
					}
				}
			}
		}
		
		return $paymentMethods;
	}	
	
	/**
	 * get all payment methods
	 *		 
	 * @return \array $paymentMethods
	 */
	public function getAllPaymentMethods(){
		
		// initialize
		$this->initialize();
		$paymentMethods = array();
		
		if(ExtensionManagementUtility::isLoaded('moox_payment_voucher')){			
			$paymentProvider = $this->objectManager->get('DCNGmbH\MooxPaymentVoucher\Service\PaymentProvider');		
			if(is_object($paymentProvider)){
				$paymentMethods['moox_payment_voucher'] = $paymentProvider->getPaymentConfig();
			}
		}
		if(ExtensionManagementUtility::isLoaded('moox_payment_vrpay')){			
			$paymentProvider = $this->objectManager->get('DCNGmbH\MooxPaymentVrpay\Service\PaymentProvider');		
			if(is_object($paymentProvider)){
				$paymentMethods['moox_payment_vrpay'] = $paymentProvider->getPaymentConfig();
			}
		}
		
		return $paymentMethods;
	}
}
?>